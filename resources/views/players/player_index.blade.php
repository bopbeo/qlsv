<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Quản lý cầu thủ</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>  
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

  <nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <b class="navbar-brand" style="color: #FF0066">WebSport.vn</b>
      </div>
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="{{url('login')}}" class="dropdown-toggle" data-toggle="dropdown">{{Auth::user()->email}}<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="{{url('logout')}}">Logout</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-left">
          <li style="border-left: 1px solid #ddd"><a href="/clubs"><b>Club</b></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-left">
          <li style="border-left: 1px solid #ddd; border-right: 1px solid #ddd"><a href="/players"><b>Player</b></a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container">    
    <br />
    <h3 align="center">Danh sách các cầu thủ</h3>
    <br />
    <div align="right">
      <button type="button" name="create_record" id="create_record" class="btn btn-success btn-sm">Create Record</button>
    </div>
    <br />
    <div class="table-responsive">
      <table class="table table-bordered table-striped" id="player_table">
        <thead>
          <tr>
            <th width="25%">Name player</th>
            <th width="20%">Age</th>
            <th width="20%">Location</th>
            <th width="20%">ID club</th>
            <th width="15%">Action</th>
          </tr>
        </thead>
      </table>
    </div>
    <br />
    <br />
  </div>
</body>
</html>

@include ('players.player_add')
@include ('players.player_delete')

<script src="{{url('js/player.js')}}"></script>