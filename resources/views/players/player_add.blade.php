<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title"></h4>
    </div>

    <div class="modal-body">
      <span id="form_result"></span>
      <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label class="control-label col-md-4" >Name player : </label>
          <div class="col-md-8">
            <input type="text" name="name" id="name" class="form-control" />
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4">Age :</label>
          <div class="col-md-8">
            <input type="number" name="age" id="age" class="form-control" />
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4">Location : </label>
          <div class="col-md-8">
            <input type="text" name="location" id="location" class="form-control" />
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4">ID club : </label>
          <div class="col-md-8">
            <?php $data = App\Models\InfoClub::select('id')->get() ?>
            <select class="form-control" name="club_id" id="club_id">
              @foreach ($data as $dt)
              <option>{{$dt->id}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <br />
        <div class="form-group" align="center">
          <input type="hidden" name="action" id="action" />
          <input type="hidden" name="hidden_id" id="hidden_id" />
          <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
        </div>
      </form>
    </div>
  </div>
</div>
</div>