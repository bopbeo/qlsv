<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-4">
				<form action="{{url('login')}}" method="POST" role="form">
					@csrf
					@if(session(('message')))
					<p style="color: red">{{session(('message'))}}</p>
					@endif
					<legend class="col-xs-7" style="text-align: center;"><b>Login</b></legend>
					<div class="input-group col-xs-7">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input id="email" type="text" class="form-control" name="email" placeholder="Email">  
					</div>
					@if($errors->has('email'))
					<p style="color: red">{{$errors->first('email')}}</p>
					@endif
					<br />
					<div class="input-group col-xs-7">
						<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
						<input id="password" type="password" class="form-control" name="password" placeholder="Password">
					</div>
					@if($errors->has('password'))
					<p style="color: red">{{$errors->first('password')}}</p>
					@endif
					<br /><br />
					<button type="submit" class="btn btn-primary">Đăng nhập</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>