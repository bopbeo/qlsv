<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        App\User::create([
            'name' => 'anhha',
            'email' =>'anhha@gmail.com',
            'type' => '1',
            'password' => bcrypt('1111')
        ]);
    }
}
