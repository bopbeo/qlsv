$(document).ready(function() {

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $('#club_table').DataTable({
    processing: true,
    serverSide: true,
    ajax:{
      url:'clubs',
    },
    columns:[
      {
        data: 'name'
      },
      {
        data: 'nation'
      },
      {
        data: 'year'
      },
      {
        data: 'action',
        'render': function (data, type, row) 
        {
          btn_edit = '<button type="button" name="edit" id="'+row['id']+'" class="edit btn btn-primary btn-sm">Edit</button>';
          btn_delete = '&nbsp&nbsp<button type="button" name="delete" id="' + row['id'] + '" class="delete btn btn-danger btn-sm">Delete</button>';
          return btn_edit + btn_delete;
        },
        orderable: false
      }
    ]
  });

  $('#create_record').click(function(){
   $('.modal-title').text("Add New Record");
   $('#action_button').val("Add");
   $('#action').val("Add");
   $('#formModal').modal('show');
 });

  $(document).on('click', '.edit', function(){
    var id = $(this).attr('id');
    $('#form_result').html('');
    $.ajax({
      url:"/clubs/"+id+"/edit",
      dataType:"json",
      success:function(html){
        $('#name').val(html.name);
        $('#nation').val(html.nation);
        $('#year').val(html.year);
        $('#hidden_id').val(html.id);
        $('.modal-title').text("Edit Record");
        $('#action_button').val("Edit");
        $('#action').val("Edit");
        $('#formModal').modal('show');
      }
    })
  });

  $('#sample_form').on('submit', function(event){
    event.preventDefault();
    var id = $(this).attr('id');
    //add
    if($('#action').val() == 'Add')
    {
      $.ajax({
        url:'clubs',
        method:"POST",
        data: new FormData(this),
        contentType: false,
        cache:false,
        processData: false,
        dataType:"json",
        success:function(data)
        {
          $('#sample_form')[0].reset();
          $('#club_table').DataTable().ajax.reload();
          $('#formModal').modal('hide');
          toastr.success('data.success');
        },
        error:function(data)
        {
          var er = $.parseJSON(data.responseText);
          $.each(er.errors, function(key, value){
            toastr.error(value);
          });
        }
      })
    }
    //end add
    
    //edit
    if($('#action').val() == "Edit")
    {
      $.ajax({
        url:"clubs/" + id,
        method:"PUT",
        data:$('#sample_form').serialize(),
        cache: false,
        processData: false,
        dataType:"json",
        success:function(data)
        {
          $('#sample_form')[0].reset();
          $('#club_table').DataTable().ajax.reload();
          $('#formModal').modal('hide');
          toastr.success('data.success')
        },
        error:function(data)
        {
          var er = $.parseJSON(data.responseText);
          $.each(er.errors, function(key, value){
            toastr.error(value);
          });
        }
      });
    }
    //end edit
  });

  //delete
  var id;

  $(document).on('click', '.delete', function(){
    id = $(this).attr('id');
    $('#confirmModal').modal('show');
  });

  $('#ok_button').click(function(){
    $.ajax({
      url:"clubs/" + id,
      method:"DELETE",
      beforeSend:function()
      {
        $('#ok_button').text('Deleting...');
      },
      success:function(data)
      {
        setTimeout(function()
        {
          $('#confirmModal').modal('hide');
          $('#club_table').DataTable().ajax.reload();
        }, 2000);
      }
    })
  });
  //end delete
});

