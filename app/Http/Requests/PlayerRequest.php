<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlayerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'age' => 'required',
            'location' => 'required',
            'club_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name player không được để trống',
            'age.required' => 'Age không được để trống',
            'location.required' => 'Location không được để trống',
            'club_id.required' => 'ID club không được để trống'
        ];
    }
}
