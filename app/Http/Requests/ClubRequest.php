<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClubRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'nation' => 'required',
            'year' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name Club không được để trống',
            'nation.required' => 'Nation không được để trống',
            'year.required' => 'Founded Year không được để trống'
        ];
    }
}
