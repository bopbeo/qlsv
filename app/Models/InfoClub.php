<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoClub extends Model
{
    protected $table = 'clubs';

    protected $fillable = [
    	'name', 'nation', 'year',
    ];

    public function players()
    {
        return $this->hasMany('App\Models\InfoPlayer');
    }

    public static function showEditClub($id)
    {   
        try {
            $club = InfoClub::find($id);
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
    	return response()->json($club);
    }

    public static function updateClub($request)
    {
        try {
            $formData = array(
                'name' => $request->name,
                'nation' => $request->nation,
                'year' => $request->year
            );
            InfoClub::whereId($request->hidden_id)->update($formData);
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
        return response()->json(['success' => 'Data is successfully updated']);
    }

    public static function store($request)
    {
        try {
            $formData = array(
                'name' =>  $request->name,
                'nation' =>  $request->nation,
                'year' =>  $request->year
            );
            InfoClub::create($formData);
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
        return response()->json(['success' => 'Data Added successfully.']);
    }

    public static function destroyClub($id)
    {
        try {
            $data = InfoClub::find($id);
            $data->delete($id);
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
    }
}
