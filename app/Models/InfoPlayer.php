<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoPlayer extends Model
{
	protected $table = 'players';

	protected $fillable = [
		'name', 'age', 'location', 'club_id'
	];

	public function clubs() 
	{
		return $this->belongsTo('App\Models\InfoClub', 'club_id', 'id');
	}

	public static function store($request)
	{
		try {
			$formData = array(
				'name' =>  $request->name,
				'age' =>  $request->age,
				'location' =>  $request->location,
				'club_id' => $request->club_id
			);
			InfoPlayer::create($formData);
		} catch (Exception $e) {
			return response()->json(['error' => $e]);
		}
		return response()->json(['success' => 'Data Added successfully.']);
	}

	public static function showEditPlayer($id)
    {
    	try {
    		$player = InfoPlayer::find($id);
    	} catch (Exception $e) {
    		return response()->json(['error' => $e]);
    	}
    	return response()->json($player);
    }

    public static function updatePlayer($request)
    {
    	try {
    		$formData = array(
				'name' =>  $request->name,
				'age' =>  $request->age,
				'location' =>  $request->location,
				'club_id' => $request->club_id
			);
        	InfoPlayer::whereId($request->hidden_id)->update($formData);
    	} catch (Exception $e) {
    		return response()->json(['error' => $e]);
    	}
        return response()->json(['success' => 'Data is successfully updated']);
    }

    public static function destroyPlayer($id)
    {
    	try {
    		$data = InfoPlayer::find($id);
        	$data->delete($id);
    	} catch (Exception $e) {
    		return response()->json(['error' => $e]);
    	}
    }
}